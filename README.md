# State Channels - Edcon Hack Slides

Build your first state channel using Solidity. These are the slides for Edcon.io Hack Workshop session.

[See Live Slides]()

## Development

`npm i` - install

`npm start` - local build & development

`npm run build` - build for deploy
